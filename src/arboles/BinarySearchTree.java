package arboles;

public class ArbolBinarioBusqueda {
	private Nodo<Integer> raiz;

	public ArbolBinarioBusqueda() {
		raiz = null;
	}

	public boolean vacio() {
		return raiz == null;
	}

	public void insertar(Integer dato) {
		if (vacio()) {
			raiz = new Nodo<Integer>(dato);
		} else {
			insertar(dato, raiz);
		}
	}

	private Nodo<Integer> insertar(Integer dato, Nodo<Integer> nodo) {
		if (nodo == null) {
			return new Nodo<Integer>(dato);
		}
		if (dato.compareTo(nodo.getInfo()) > 0) {
			nodo.setDer(insertar(dato, nodo.getDer()));
		}
		if (dato.compareTo(nodo.getInfo()) < 0) {
			nodo.setIzq(insertar(dato, nodo.getIzq()));
		}
		return nodo;
	}

	public boolean pertenece(Integer dato) {
		if (vacio()) {
			return false;
		}
		return pertenece(dato, raiz);
	}

	// si esta balanceado entonces siempre es O(log2(n))
	private boolean pertenece(Integer dato, Nodo<Integer> nodo) { // O(log2(n)), no siempre voy a tener esto. Aunque por lo
																// general en menor que O(n)
		if (nodo == null) {
			return false;
		}
		if (nodo.getInfo().equals(dato)) {
			return true;
		}
		if (nodo.getInfo().compareTo(dato) > 0) {
			return pertenece(dato, nodo.getIzq());
		}
		return pertenece(dato, nodo.getDer());
	}

	public Integer altura() {
		if (vacio()) {
			return 0;
		}
		return altura(raiz);
	}

	private Integer altura(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		return Math.max(altura(nodo.getIzq()), altura(nodo.getDer())) + 1;
	}

	public Integer sumarValorNodos() {
		if (vacio()) {
			return 0;
		}
		return sumarValorNodos(raiz);
	}

	private Integer sumarValorNodos(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		return nodo.getInfo() + sumarValorNodos(nodo.getIzq()) + sumarValorNodos(nodo.getDer());
	}

	public Integer minimo() {
		if (vacio()) {
			return null;
		}
		return minimo(raiz);
	}

	private Integer minimo(Nodo<Integer> nodo) {
		if (nodo.getIzq() == null) {
			return nodo.getInfo();
		}
		return minimo(nodo.getIzq());
	}

	public Integer maximo() {
		if (vacio()) {
			return null;
		}
		return maximo(raiz);
	}

	private Integer maximo(Nodo<Integer> nodo) {
		if (nodo.getDer() == null) {
			return nodo.getInfo();
		}
		return maximo(nodo.getDer());
	}

	public float promedio() {
		if (vacio()) {
			return 0;
		}
		return sumarValorNodos() / cantidadNodos();
	}

	public Integer cantidadNodos() {
		if (vacio()) {
			return 0;
		}
		return cantidadNodos(raiz);
	}

	private Integer cantidadNodos(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		return 1 + cantidadNodos(nodo.getIzq()) + cantidadNodos(nodo.getDer());
	}
	
	public Integer cantidadNodosNoHojas() {
		if (vacio()) {
			return 0;
		}
		return cantidadNodosNoHojas(raiz);
	}
	
	private Integer cantidadNodosNoHojas(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (esUnaHoja(nodo)) {
			return 0;
		}
		return 1 + cantidadNodosNoHojas(nodo.getIzq()) + cantidadNodosNoHojas(nodo.getDer());
	}

//	public Nodo<Integer> iesimo(int pos) {
//		if(vacio()) {
//			return null;
//		}
//		return iesimo(raiz, pos, 0);
//	}
//	
//	
//	private Nodo<Integer> iesimo(Nodo<Integer> nodo, int pos, int nivel) {
//		if (nodo == null) {
//			return null;
//		}
//
//		Nodo<Integer> izq = iesimo(nodo.getIzq(), pos, nivel);
//		++nivel; //crear metodo contador y pasarlo por parametro, para que funcione
//		if (izq != null) {
//			return izq;
//		}
//		if (nivel == pos) {
//			return nodo;
//		}
//		++nivel;
//		return iesimo(nodo.getDer(), pos, nivel);
//	}

	// empieza desde la pos 0
	public Integer iesimo(int pos) {
		if (pos < 0 || pos >= cantidadNodos() || vacio()) {
			return null;
		}
		return iesimo(pos, raiz);
	}

	private Integer iesimo(int pos, Nodo<Integer> nodo) {
		int cantIzq = cantidadNodos(nodo.getIzq());
		// caso base: retorno la info del nodo
		if (pos == cantIzq)
			return nodo.getInfo();
		if (pos < cantIzq)
			// llamada recursiva sobre el subarbol izquierdo
			// la pos queda igual
			return iesimo(pos, nodo.getIzq());
		else
			// llamada recursiva sobre el subarbol derecho
			// a la pos le resto la cantidad de nodos del
			// subarbol izq + 1 (la raiz)
			return iesimo(pos - (cantIzq + 1), nodo.getDer());
	}

	public void eliminar(Integer elem) {
		raiz = eliminar(elem, raiz);
	}

	private Nodo<Integer> eliminar(Integer elem, Nodo<Integer> nodo) {
		if (nodo == null)
			return null;
		if (elem.compareTo(nodo.getInfo()) < 0)
			nodo.setIzq(eliminar(elem, nodo.getIzq()));
		else if (elem.compareTo(nodo.getInfo()) > 0)
			nodo.setDer(eliminar(elem, nodo.getDer()));
		else {
			// encontre el nodo a borrar
			if (nodo.getIzq() == null)
				return nodo.getDer();
			if (nodo.getDer() == null)
				return nodo.getIzq();

			// caso dificil: nodo tiene 2 hijos
			Integer minDer = minimo(nodo.getDer());
			nodo.setInfo(minDer);
			nodo.setDer(eliminar(minDer, nodo.getDer()));
		}
		return nodo;
	}
	
	private boolean tieneUnHijo(Nodo<Integer> nodo) {
		if (nodo.getDer() != null && nodo.getIzq() == null) {
			return true;
		}
		if (nodo.getDer() == null && nodo.getIzq() != null) {
			return true;
		}
		return false;
	}
	
	private boolean esUnaHoja(Nodo<Integer> nodo) {
		return nodo.getIzq() == null && nodo.getDer() == null;
	}
	
	public Integer cantNodosConHijos() {
		if (vacio()) {
			return 0;
		}
		return cantNodosConHijos(raiz);
	}

	private Integer cantNodosConHijos(Nodo<Integer> nodo) {
		if(nodo == null) {
			return 0;
		}
		if(esUnaHoja(nodo)) { //las hojas no tienen hijos
			return 0;
		}
		return 1 + cantNodosConHijos(nodo.getIzq()) + cantNodosConHijos(nodo.getDer());
	}
	
	public Integer cantNodosConUnHijo() {
		if (vacio()) {
			return 0;
		}
		return cantNodosConUnHijo(raiz); 
	}
	
	private Integer cantNodosConUnHijo(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (tieneUnHijo(nodo)) {
			return 1 + cantNodosConUnHijo(nodo.getIzq()) + cantNodosConUnHijo(nodo.getDer());
		}
		return cantNodosConUnHijo(nodo.getIzq()) + cantNodosConUnHijo(nodo.getDer());
	}

	public Integer sumarDeRaizAHoja() {
		if (vacio()) {
			return 0;
		}
		return sumarDeRaizAHoja(raiz, 0);
	}
	
	private Integer sumarDeRaizAHoja(Nodo<Integer> nodo, int cont) {
		if (nodo == null) {
			return 0;
		}
		cont = cont * 10 + nodo.getInfo();
		if (esUnaHoja(nodo)) {
			return cont;
		}
		return sumarDeRaizAHoja(nodo.getIzq(), cont) + sumarDeRaizAHoja(nodo.getDer(), cont);
	}
	
	public Integer sumarMayorAElemento(Integer dato) {
		if (vacio()) {
			return 0;
		}
		return sumarMayorAElemento(dato, raiz);
	}

	private Integer sumarMayorAElemento(Integer dato, Nodo<Integer> nodo) {
		int res = 0;
		if (nodo == null) {
			return 0;
		}
		if (nodo.getInfo() > dato) {
			res += nodo.getInfo(); 
		}
		res += sumarMayorAElemento(dato, nodo.getDer());
		res += sumarMayorAElemento(dato, nodo.getIzq());
		return res;
	}
	
	public Integer mayoresNoHojas(Integer dato) {
		if (vacio()) {
			return 0;
		}
		return mayoresNoHojas(dato, raiz);
	}

	//si info es mayor que dato y no es una hoja lo sumo, sino avanzo. Aclaracion:para que no sea una hoja, debe tener al menos un hijo.
	private Integer mayoresNoHojas(Integer dato, Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (nodo.getInfo() > dato && tieneAlMenosUnHijo(nodo)) {
			return 1 + mayoresNoHojas(dato, nodo.getDer()) + mayoresNoHojas(dato, nodo.getIzq());
		}
		return mayoresNoHojas(dato, nodo.getDer());
	}
	
	private boolean tieneAlMenosUnHijo(Nodo<Integer> nodo) {
		return nodo.getIzq() != null || nodo.getDer() != null;
	}
	
	//---------------------------------------------
	public Integer mayoresNoHojas2(Integer dato) {
		if (vacio()) {
			return 0;
		}
		return mayoresNoHojas2(dato, raiz);
	}

	//otra forma
	private Integer mayoresNoHojas2(Integer dato, Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (nodo.getInfo().compareTo(dato) > 0 && (nodo.getDer() != null || nodo.getIzq() != null)) {
			return 1 + mayoresNoHojas2(dato, nodo.getIzq()) + mayoresNoHojas2(dato, nodo.getDer());
		}
		return mayoresNoHojas2(dato, nodo.getDer());
	}
	
	public Integer sumarPares() {
		if (vacio()) {
			return null;
		}
		return sumarPares(raiz);
	}
	

	private Integer sumarPares(Nodo<Integer> nodo) { 
		if (nodo == null) {
			return 0;
		}
		if (esPar(nodo.getInfo())) {
			return nodo.getInfo() + sumarPares(nodo.getIzq()) + sumarPares(nodo.getDer()); 
		}
		return sumarPares(nodo.getIzq()) + sumarPares(nodo.getDer()); 	
	}
	
	private boolean esPar(int n) {
		return n % 2 == 0;
	}
	
	public Integer sumarImpares() {
		if (vacio()) {
			return null;
		}
		return sumarImpares(raiz);
	}
	

	private Integer sumarImpares(Nodo<Integer> nodo) { 
		if (nodo == null) {
			return 0;
		}
		if (!esPar(nodo.getInfo())) {
			return nodo.getInfo() + sumarImpares(nodo.getIzq()) + sumarImpares(nodo.getDer()); 
		}
		return sumarImpares(nodo.getIzq()) + sumarImpares(nodo.getDer()); 	
	}
	
	public Integer sumarElementosEntre(Integer elem1, Integer elem2) {	
		if (!pertenece(elem1) || !pertenece(elem2) || elem1.equals(elem2)) {
			return 0;
		}
		if (elem1.compareTo(elem2) > 0) {
			return sumarElementosEntre(elem2, elem1, raiz);
		}
		return sumarElementosEntre(elem1, elem2, raiz); 
	}

	private Integer sumarElementosEntre(Integer elem1, Integer elem2, Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		int sum = 0;
		if (elem1.compareTo(nodo.getInfo()) <=0 && elem2.compareTo(nodo.getInfo()) >= 0) {
			sum = nodo.getInfo();
		}
		return sumarElementosEntre(elem1, elem2, nodo.getIzq()) + sum + sumarElementosEntre(elem1, elem2, nodo.getDer());
	}
	
	//otra forma
//	private Integer sumarElementosEntre(Integer elem1, Integer elem2, Nodo<Integer> nodo) {
//		if (nodo == null) {
//			return 0;
//		}
//		if (elem1.compareTo(nodo.getInfo()) <=0 && elem2.compareTo(nodo.getInfo()) >= 0) {
//			return sumarElementosEntre(elem1, elem2, nodo.getIzq()) + nodo.getInfo() + sumarElementosEntre(elem1, elem2, nodo.getDer());
//		}
//		return sumarElementosEntre(elem1, elem2, nodo.getIzq()) + sumarElementosEntre(elem1, elem2, nodo.getDer());
//	}
	
	//suma del elemento a la raiz cuantos nodos hay, lo mismo para ambos elementos
	//basicamente, seria la cantidad de nodos que hay entre un elemento, para llegar a el otro
	public Integer elementosPasandoPorRaiz(Integer elem1, Integer elem2) {	
		if (!pertenece(elem1) || !pertenece(elem2) || vacio()) {
			return 0;
		}
		return elementosPasandoPorRaiz(elem1, raiz) + elemsPasandoPorRaiz(elem1, raiz) - 1; 
	}

	private Integer elementosPasandoPorRaiz(Integer dato, Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (dato.equals(nodo.getInfo())) { //si es igual no lo cuento
			return 0; 
		}
		if (dato > nodo.getInfo()) {
			return elementosPasandoPorRaiz(dato, nodo.getDer()) + 1;
		}
		return elementosPasandoPorRaiz(dato,nodo.getIzq()) + 1;
	}
	
	//devuelve la cantidad de elementos que estan entre medio 
	//de los dos elementos pasados por parametro
	public Integer elementosEntreMedio(Integer elem1, Integer elem2) {
		if (!pertenece(elem1) || !pertenece(elem2) || vacio() || elem1.equals(elem2)) {
			return 0;
		}
		if (elem1.compareTo(elem2) > 0) {
			return elementosEntreMedio(elem2, elem1, raiz);
		}
		return elementosEntreMedio(elem1, elem2, raiz);
	}
	
	private Integer elementosEntreMedio(Integer elem1, Integer elem2, Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (elem1.compareTo(nodo.getInfo()) < 0 && elem2.compareTo(nodo.getInfo()) > 0) {
			return 1 + elementosEntreMedio(elem1, elem2, nodo.getIzq()) + elementosEntreMedio(elem1, elem2, nodo.getDer());
		}
		return elementosEntreMedio(elem1, elem2, nodo.getIzq()) + elementosEntreMedio(elem1, elem2, nodo.getDer());
	}
	
	public String mostrarRecorrido(Integer elem) {
        if(vacio())
            return "";
        if(this.raiz.getInfo() == elem)
            return this.raiz.getInfo().toString();
        return mostrarRecorrido(elem, this.raiz);
    }

    private String mostrarRecorrido(Integer elem, Nodo<Integer> nodo) {
        if(nodo == null)
            return "";
        if(nodo.getInfo()==elem)
            return nodo.getInfo().toString()+" ";
        if(elem < nodo.getInfo())
            return nodo.getInfo().toString() + " " + mostrarRecorrido(elem, nodo.getIzq());

        return nodo.getInfo().toString() + " " + mostrarRecorrido(elem, nodo.getDer());
    }
    
    public String elemDesde(Integer elem) {
        if(vacio())
            return "";
        if(this.raiz.getInfo() == elem)
            return this.raiz.getInfo().toString();
        return elemDesde(elem, this.raiz);
    }
    
    private String elemDesde(Integer elem, Nodo<Integer> nodo) {
        if(nodo == null)
            return "";
        if(nodo.getInfo() == elem)
            return nodo.getInfo().toString();
        
        if(elem < nodo.getInfo())
           return elemDesde(elem, nodo.getIzq()) + " " + nodo.getInfo();

        return  elemDesde(elem, nodo.getDer()) + " " + nodo.getInfo();
    }
    
    //arreglar
//    public String mostrarRecorridoEntre(Integer elem1, Integer elem2) {
//        if(vacio())
//            return "";
//        if(this.raiz.getInfo() == elem1 || this.raiz.getInfo() == elem2)
//            return this.raiz.getInfo().toString();
//        if (elem1.compareTo(elem2) > 0) {
//			return mostrarRecorridoEntre(elem2, elem1, raiz);
//		}
//        return mostrarRecorridoEntre(elem1, elem2, this.raiz);
//    }
//
//    private String mostrarRecorridoEntre(Integer elem1, Integer elem2, Nodo<Integer> nodo) {
//        if (nodo == null)
//            return " ";
//        if (nodo.getInfo().equals(elem1)) {
//        	return " " + nodo.getInfo().toString() + " ";
//        }
//        if (nodo.getInfo().equals(elem2)) {
//        	return " " + nodo.getInfo().toString() + " ";
//        }
//        return " " + mostrarRecorridoEntre(elem1, elem2, nodo.getIzq()) + " " +nodo.getInfo() + " " +mostrarRecorridoEntre(elem1, elem2, nodo.getDer()); 
//    }
    
    public boolean arbolLleno() {
    	if (vacio()) {
    		return false;
    	}
    	return arbolLleno(raiz);
    }

	private boolean arbolLleno(Nodo<Integer> nodo) {
		if (nodo == null) {
			return true;
		}
		if (nodo.getDer() == null && nodo.getIzq() == null) {
			return true;
		}
		if (nodo.getDer() != null && nodo.getIzq() != null) {
			return arbolLleno(nodo.getIzq()) && arbolLleno(nodo.getDer());
		}
		return false;
	}
	
	public String mayoresInternos(Integer dato) {
		if (vacio()) {
			return "";
		}
		return mayoresInternos(dato, raiz);
	}

	private String mayoresInternos(Integer dato, Nodo<Integer> nodo) {
		if (nodo == null) {
			return " ";
		}
		if (nodo.getInfo().compareTo(dato) > 0 && (nodo.getDer() != null || nodo.getIzq() != null)) {
			return "" + mayoresInternos(dato, nodo.getIzq()) + nodo.getInfo().toString() + mayoresInternos(dato, nodo.getDer());
		}
		return mayoresInternos(dato, nodo.getDer());
	}

	// imprime en orden
	public String toString(Nodo<Integer> nodo) {
		if (nodo == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		sb.append(toString(nodo.getIzq()));
		sb.append(" ");
		sb.append(nodo.getInfo());
		sb.append(" ");
		sb.append(toString(nodo.getDer()));

		return sb.toString();
	}
	
	public String preOrden() {
		return "{" + toStringPreOrden(raiz) + "}";
	}

	private String toStringPreOrden(Nodo<Integer> nodo) {
		StringBuilder sb = new StringBuilder();
		if (nodo != null) {
			sb.append(nodo.getInfo()).append(" ");
			sb.append(toStringPreOrden(nodo.getIzq()));
			sb.append(toStringPreOrden(nodo.getDer()));
		}
		return sb.toString();
	}

	public Nodo<Integer> getRaiz() {
		return raiz;
	}

	public static void main(String[] args) {
		ArbolBinarioBusqueda arbolito = new ArbolBinarioBusqueda();
		arbolito.insertar(18);
		arbolito.insertar(12);
		arbolito.insertar(39);
		arbolito.insertar(5);
		arbolito.insertar(3);
		arbolito.insertar(10);
//		arbolito.insertar(45);
//		arbolito.insertar(40);
//		arbolito.insertar(50);
//		arbolito.insertar(51);
//		arbolito.insertar(5);
//		arbolito.insertar(4);
//		arbolito.insertar(-1);
//		arbolito.insertar(-2);
//		arbolito.insertar(6);
//		arbolito.insertar(7);
//		arbolito.insertar(5);
//		arbolito.insertar(9);
//		arbolito.insertar(0);
//		arbolito.insertar(11);
		
//		System.out.println(arbolito.toString(arbolito.getRaiz()));
//		System.out.println(arbolito.preOrden());
//		System.out.println(arbolito.buscar(6));
//		System.out.println(arbolito.altura());
//		System.out.println(arbolito.sumarValorNodos());
//		System.out.println(arbolito.minimo());
//		System.out.println(arbolito.maximo());
//		System.out.println(arbolito.iesimo(0));
//		System.out.println(arbolito.toString(arbolito.getRaiz()));
//		System.out.println(arbolito.cantNodosConHijos());
//		System.out.println(arbolito.cantNodosConUnHijo());
//		System.out.println(arbolito.sumarDeRaizAHoja());
//		System.out.println(arbolito.sumarPares());
//		System.out.println(arbolito.sumarMayorAElemento(7));
//		System.out.println(arbolito.mayoresNoHojas2(40));
//		System.out.println(arbolito.mayoresNoHojas(11));
//		System.out.println(arbolito.cantidadNodosNoHojas());
//		System.out.println(arbolito.sumarImpares());
//		System.out.println(arbolito.sumarElemsEntre(5, 18));
//		System.out.println(arbolito.sumaElemsEntre(5,  45));
//		System.out.println(arbolito.elemsPasandoPorRaiz(3, 40));
//		System.out.println(arbolito.elemsEntre(12, 18));
		System.out.println(arbolito.mostrarRecorrido(5));
//		System.out.println(arbolito.mostrarRecorridoEntre(5, 3));
//		System.out.println(arbolito.arbolLleno());
//		System.out.println(arbolito.mayoresInternos(10));
	}
}
