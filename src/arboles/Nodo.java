package arboles;

public class Nodo<T> {

	private T info;
	private Nodo<T> izq;
	private Nodo<T> der;

	public Nodo(T info) {
		this.info = info;
	}

	public Nodo<T> getIzq() {
		return izq;
	}

	public Nodo<T> getDer() {
		return der;
	}

	// Setters para el arbol binario
	public void setIzq(Nodo<T> izq) {
		this.izq = izq;
	}

	public void setDer(Nodo<T> der) {
		this.der = der;
	}

	public T getInfo() {
		return info;
	}

	public void setInfo(T info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return info.toString();
	}

}
