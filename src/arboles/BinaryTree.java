package arboles;

public class ArbolBinario {
	private Nodo<Integer> raiz;

	public ArbolBinario() {
		raiz = null;
	}

	public boolean vacio() {
		return raiz == null;
	}

	// insertar elementos
	public void insertar(Integer dato) {
		if (vacio()) {
			raiz = new Nodo<Integer>(dato);
		} else {
			insertar(dato, raiz);
		}
	}

	private void insertar(Integer dato, Nodo<Integer> nodo) {
		if (nodo.getIzq() == null) {
			nodo.setIzq(new Nodo<Integer>(dato));
		} else if (nodo.getDer() == null) {
			nodo.setDer(new Nodo<Integer>(dato));
		} else {
			insertar(dato, nodo.getIzq());
		}
	}

	// buscar un elemento, para saber si pertenece o no al arbol
	public boolean pertenece(Integer dato) {
		if (vacio()) {
			return false;
		}
		return pertenece(dato, raiz);
	}

	private boolean pertenece(Integer dato, Nodo<Integer> nodo) { // O(n) --> recorro todo
		if (nodo == null) {
			return false;
		}
		if (nodo.getInfo().equals(dato)) {
			return true;
		}
		return pertenece(dato, nodo.getDer()) || pertenece(dato, nodo.getIzq());
	}

	// cantidad de hojas que tiene el arbol
	public Integer cantidadDeHojas() {
		if (vacio()) {
			return 0;
		}
		return cantidadDeHojas(raiz);
	}

	private Integer cantidadDeHojas(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (esUnaHoja(nodo)) {
			return 1;
		}
		return cantidadDeHojas(nodo.getIzq()) + cantidadDeHojas(nodo.getDer());
	}

	private boolean esUnaHoja(Nodo<Integer> nodo) {
		return nodo.getIzq() == null && nodo.getDer() == null;
	}

	// cantidad total de nodos
	public Integer cantidadDeNodos() {
		if (vacio()) {
			return 0;
		}
		return cantidadDeNodos(raiz);
	}

	private Integer cantidadDeNodos(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		return 1 + cantidadDeNodos(nodo.getIzq()) + cantidadDeNodos(nodo.getDer());
	}

	//cada operacion de altura() es O(1)
	//se produce una llamada recursiva por cada nodo
	//entonces se produce O(1).n = O(n)
	
	// altura del arbol
	public Integer altura() {
		if (vacio()) {
			return 0;
		}
		return altura(raiz);
	}

	private Integer altura(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		return Math.max(altura(nodo.getIzq()), altura(nodo.getDer())) + 1;
	}

	public void quitar(Integer dato) {
		if (!vacio()) {
			quitar(dato, raiz);
		}
	}

	// si queremos eliminar un nodo que tiene hijos, enganchamos una hoja en su
	// lugar
	// y se elimina el valor que tenia antes, a la hoja hay que ponerla en NULL
	private void quitar(Integer dato, Nodo<Integer> nodo) {
		if (nodo != null) {
			if (esUnaHoja(nodo)) {

			}
		}
	}

	// sumar todos los nodos
	public Integer suma() {
		if (vacio()) {
			return 0;
		}
		return suma(raiz);
	}

	private Integer suma(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		return nodo.getInfo() + suma(nodo.getIzq()) + suma(nodo.getDer());
	}

	// Retorna si el arbol esta o no balanceado
	public boolean balanceado() {
		return balanceado(raiz);
	}

	private boolean balanceado(Nodo<Integer> nodo) {
		if (nodo == null)
			return true;
		return Math.abs(this.altura(nodo.getIzq()) - this.altura(nodo.getDer())) <= 1 && balanceado(nodo.getIzq())
				&& balanceado(nodo.getDer());
	}

	// Retorna un String con los valores de las hojas
	public String valoresHojas() {
		return "{" + valoresHojas(raiz) + "}";
	}

	private String valoresHojas(Nodo<Integer> nodo) {
		if (nodo == null) {
			return "";
		}
		if (esUnaHoja(nodo)) {
			return nodo.getInfo().toString() + " ";
		}

		return valoresHojas(nodo.getIzq()) + valoresHojas(nodo.getDer());
	}

	// posicion de un elemento
	public Integer iesimo(int pos) {
		if (pos < 0 || pos >= cantidadDeNodos()) { // miro si es un i valido
			return null;
		}
		return iesimo(pos, raiz);
	}

	private Integer iesimo(int i, Nodo<Integer> nodo) {
		int cantIzq = cantidadDeNodos(nodo.getIzq());
		if (i == cantIzq)

			// caso base: retorno la info del nodo
			return nodo.getInfo();
		if (i < cantIzq)
			// llamada recursiva sobre el subarbol izquierdo
			// el i queda igual
			return iesimo(i, nodo.getIzq());
		else
			// llamada recursiva sobre el subarbol derecho
			// al i le resto la cantidad de nodos del
			// subarbol izq + 1 (la raiz)
			return iesimo(i - (cantIzq + 1), nodo.getDer());
	}

	public Integer minimo() {
		if (vacio()) {
			return null;
		}
		return minimo(raiz);
	}

	private Integer minimo(Nodo<Integer> nodo) {
		if (nodo == null) {
			return Integer.MAX_VALUE;
		} 
		return min(nodo.getInfo(),minimo(nodo.getIzq()), minimo(nodo.getDer()));
	}
	
	private Integer min(Integer info, Integer izq, Integer der) {
		if(info < izq && info < der) {
			return info;
		}
		if(izq < info && izq < der) {
			return izq;
		}
		return der;
	}
	
	//otra forma
	private Integer minimo2(Nodo<Integer> nodo) {
		int min;
		if (nodo == null) {
			return Integer.MAX_VALUE;
		} else {
			min = nodo.getInfo();
			int izq = minimo(nodo.getIzq());
			if (izq < min) {
				min = izq;
			}

			int der = minimo(nodo.getDer());
			if (der < min) {
				min = der;
			}
		}
		return min;
	}

	public Integer maximo() {
		if (vacio()) {
			return null;
		}
		return maximo(raiz);
	}

	private Integer maximo(Nodo<Integer> nodo) {
		int max = 0;
		if (nodo == null) {
			max = Integer.MIN_VALUE;
		} else {
			max = nodo.getInfo();
			int izq = maximo(nodo.getIzq());
			if (izq > max) {
				max = izq;
			}

			int der = maximo(nodo.getDer());
			if (der > max) {
				max = der;
			}
		}
		return max;
	}
	
	public Integer sumarHojas() {
		if (vacio()) {
			return 0;
		}
		return sumarHojas(raiz);
	}

	private Integer sumarHojas(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if(esUnaHoja(nodo)) {
			return nodo.getInfo();
		}
		return sumarHojas(nodo.getDer()) + sumarHojas(nodo.getIzq());
	}
	
	public Integer sumarPadres() {
		if (vacio()) {
			return 0;
		}
		return sumarPadres(raiz);
	}

	private Integer sumarPadres(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (esUnaHoja(nodo)) {
			return 0;
		}
		return nodo.getInfo() + sumarPadres(nodo.getDer()) + sumarPadres(nodo.getIzq());
	}

	private boolean tieneHijos(Nodo<Integer> nodo) {
		return (nodo.getDer() != null && nodo.getIzq() == null) || (nodo.getDer() == null && nodo.getIzq() != null);
	}
	
	public Integer nodosConUnHijo() {
		if (vacio()) {
			return 0;
		}
		return nodosConUnHijo(raiz);
	}

	
	private Integer nodosConUnHijo(Nodo<Integer> nodo) {
		if(nodo == null) {
			return 0;
		}
		if(tieneHijos(nodo)) {
			return 1;
		}
		return nodosConUnHijo(nodo.getIzq()) + nodosConUnHijo(nodo.getDer());
	}
	
	public Integer ramaMasCorta() {
		if (vacio()) {
			return 0;
		}
		return ramaMasCorta(raiz);
	}

	private Integer ramaMasCorta(Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (esUnaHoja(nodo)) {
			return 1;
		}
		return Math.min(ramaMasCorta(nodo.getIzq()), ramaMasCorta(nodo.getDer())) + 1;
	}
	
	public Integer mayoresNoHojas(Integer dato) {
		if (vacio()) {
			return 0;
		}
		return mayoresNoHojas(dato, raiz);
	}
	
	private Integer mayoresNoHojas(Integer dato, Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (nodo.getInfo() > dato && tieneAlMenosUnHijo(nodo)) {
			return 1 + mayoresNoHojas(dato, nodo.getDer()) + mayoresNoHojas(dato, nodo.getIzq());
		}
		return mayoresNoHojas(dato, nodo.getIzq());
	}

	private boolean tieneAlMenosUnHijo(Nodo<Integer> nodo) {
		return nodo.getIzq() != null || nodo.getDer() != null;
	}
	
	public Integer cuantosMayores(Integer dato) {
		if (vacio()) {
			return 0;
		}
		return cuantosMayores(dato, raiz);
	}
	
	private Integer cuantosMayores(Integer dato, Nodo<Integer> nodo) {
		if (nodo == null) {
			return 0;
		}
		if (nodo.getInfo().compareTo(dato) > 0 ) {
			return 1 + cuantosMayores(dato, nodo.getIzq()) + cuantosMayores(dato, nodo.getDer());
		}
		return cuantosMayores(dato, nodo.getIzq()) + cuantosMayores(dato, nodo.getDer());
	}

	// in orden
	public String toString(Nodo<Integer> nodo) {
		if (nodo == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		sb.append(toString(nodo.getIzq()));
		sb.append(" ");
		sb.append(nodo.getInfo());
		sb.append(" ");
		sb.append(toString(nodo.getDer()));
		return sb.toString();
	}

	// muestra los elementos mediante un recorrido preOrden

	public String preOrden() {
		return "{" + toStringPreOrden(raiz) + "}";
	}

	private String toStringPreOrden(Nodo<Integer> nodo) {
		StringBuilder sb = new StringBuilder();
		if (nodo != null) {
			sb.append(nodo.getInfo()).append(" ");
			sb.append(toStringPreOrden(nodo.getIzq()));
			sb.append(toStringPreOrden(nodo.getDer()));
		}
		return sb.toString();
	}

	// muestra los elementos mediante un recorrido postOrden

	public String postOrden() {
		return "{" + toStringPostOrden(raiz) + "}";
	}

	private String toStringPostOrden(Nodo<Integer> nodo) {
		StringBuilder sb = new StringBuilder();
		if (nodo != null) {
			sb.append(toStringPostOrden(nodo.getIzq()));
			sb.append(toStringPostOrden(nodo.getDer()));
			sb.append(nodo.getInfo()).append(" ");
		}
		return sb.toString();
	}

	public Nodo<Integer> getRaiz() {
		return raiz;
	}

	public static int sumarElementos(int[] elem, int i) { //length = 5, i = 0
		if(i == elem.length - 1) {
           return elem[i];
        } 
        return elem[i] + sumarElementos(elem, i + 1);  
    }
	
	public static void main(String[] args) {
//		int a[] = {1, 2, 3};
//		System.out.println(sumarElementos(a, 0));
		
		ArbolBinario arbolito = new ArbolBinario();
//
		arbolito.insertar(2);
		arbolito.insertar(4);
		arbolito.insertar(1);
		arbolito.insertar(6);
		arbolito.insertar(10);
		arbolito.insertar(11);
		arbolito.insertar(7);
		arbolito.insertar(20);

//		System.out.println(arbolito.toString(arbolito.getRaiz()));
//		System.out.println(arbolito.buscar(5));
//		System.out.println(arbolito.cantidadDeHojas());
//		System.out.println(arbolito.cantidadDeNodos());
//		System.out.println(arbolito.altura());
//		System.out.println(arbolito.suma());
//		System.out.println(arbolito.minimo());
//		System.out.println(arbolito.maximo());
//		System.out.println(arbolito.iesimo(5));
//		System.out.println(arbolito.sumarHojas());
//		System.out.println(arbolito.sumarPadres());
//		System.out.println(arbolito.nodosConUnHijo());
//		System.out.println(arbolito.mayoresNoHojas(2));
//		System.out.println(arbolito.ramaMasCorta());
		System.out.println(arbolito.cuantosMayores(7));
	}

}
